# Project description
This is a simple RESTful CRUD application implemented in Java. This application can be used to operate the library. It allows you to create clients, books and available copies of the given books.

The operations that can be performed are:
- adding a user
- deleting a given user
- downloading a given user
- download all users
- adding a book
- download all books
- download the book
- deleting a given book
- adding a copy of the book
- download all copies of the book
- download copies that are available
- updating the status of a given item
- downloading a specific copy of the book
- borrow a book
- return the book
- download all loans
- downloading a specific loan
# Project objective
The main purpose of this project was to play a little bit with Spring and Hibernate frameworks, learn how to design architecture for simple RESTful web service. Learn how to write REST Controller classes and how to send HTTP requests in Java code.
# Test it live
The application has been deployed on [Heroku](https://www.heroku.com/) cloud . You can test it by yourself with [POSTMAN](https://www.getpostman.com) on this [website](https://library-738423.herokuapp.com/v1/borrower/all).
List of available endpoints under links [Swagger](https://library-738423.herokuapp.com/swagger-ui.html)
# For the future
I plan on making a frontend application page in HTML / CSS with JavaScript