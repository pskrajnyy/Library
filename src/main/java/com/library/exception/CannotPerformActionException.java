package com.library.exception;

public class CannotPerformActionException extends Exception {
    public CannotPerformActionException(String message) {
        super(message);
    }
}
