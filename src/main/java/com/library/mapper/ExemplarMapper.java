package com.library.mapper;

import com.library.domain.Exemplar;
import com.library.dto.ExemplarDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ExemplarMapper {
    @Autowired
    BorrowingMapper borrowingMapper;

    public Exemplar mapToExemplar(final ExemplarDto exemplarDto) {
        return new Exemplar(
                exemplarDto.getStatus()
        );
    }
    public ExemplarDto mapToExemplarDto(final Exemplar exemplar) {
        ExemplarDto exemplarDto = new ExemplarDto(
                exemplar.getId(),
                exemplar.getStatus(),
                borrowingMapper.mapToBorrowingDtoList(exemplar.getBorrowings())
        );
        if(exemplar.getTitle() != null) {
            exemplarDto.setTitleId(exemplar.getTitle().getId());
        }
        return exemplarDto;
    }

    public List<ExemplarDto> mapToExemplarDtoList(final List<Exemplar> exemplarList) {
        return exemplarList.stream()
                .map(this::mapToExemplarDto)
                .collect(Collectors.toList());
    }

    public List<Exemplar> mapToExemplarList(final List<ExemplarDto> exemplarDtoList) {
        return exemplarDtoList.stream()
                .map(this::mapToExemplar)
                .collect(Collectors.toList());
    }
}
