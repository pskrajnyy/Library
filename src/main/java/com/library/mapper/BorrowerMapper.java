package com.library.mapper;

import com.library.domain.Borrower;
import com.library.dto.BorrowerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class BorrowerMapper {
    @Autowired
    private BorrowingMapper borrowingMapper;

    public Borrower mapToBorrower(final BorrowerDto borrowerDto) {
        return new Borrower(
                borrowerDto.getId(),
                borrowerDto.getFirstname(),
                borrowerDto.getLastname(),
                borrowerDto.getAccountCreationDate()
        );
    }

    public BorrowerDto mapToBorrowerDto(final Borrower borrower) {
        return new BorrowerDto(
                borrower.getId(),
                borrower.getFirstName(),
                borrower.getLastName(),
                borrower.getAccountCreationDate(),
                borrowingMapper.mapToBorrowingDtoList(borrower.getBorrowings())
        );
    }

    public List<BorrowerDto> mapToBorrowerDtoList(final List<Borrower> borrowerList) {
        return borrowerList.stream()
                .map(t -> new BorrowerDto(t.getId(), t.getFirstName(), t.getLastName(), t.getAccountCreationDate(), borrowingMapper.mapToBorrowingDtoList(t.getBorrowings())))
                .collect(Collectors.toList());
    }
}
