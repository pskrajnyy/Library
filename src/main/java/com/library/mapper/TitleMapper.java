package com.library.mapper;

import com.library.domain.Title;
import com.library.dto.TitleDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TitleMapper {
    @Autowired
    ExemplarMapper exemplarMapper;

    public TitleDto mapToTitleDto(final Title title) {
        return new TitleDto(
                title.getId(),
                title.getTitle(),
                title.getAuthor(),
                title.getYearOfPublication(),
                exemplarMapper.mapToExemplarDtoList(title.getExemplars())
        );
    }

    public Title mapToTitle(final TitleDto titleDto) {
        return new Title(
                titleDto.getId(),
                titleDto.getTitle(),
                titleDto.getAuthor(),
                titleDto.getYearOfPublication(),
                exemplarMapper.mapToExemplarList(titleDto.getExemplars())
        );
    }

    public List<TitleDto> mapToTitleDtoList(final List<Title> titleList) {
        return titleList.stream()
                .map(t -> new TitleDto(t.getId(), t.getTitle(), t.getAuthor(), t.getYearOfPublication(), exemplarMapper.mapToExemplarDtoList(t.getExemplars())))
                .collect(Collectors.toList());
    }
}
