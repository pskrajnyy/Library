package com.library.mapper;

import com.library.domain.Borrowing;
import com.library.dto.BorrowingDto;
import com.library.repository.BorrowerRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class BorrowingMapper {

    public Borrowing mapToBorrowing(final BorrowingDto borrowingDto) {
        return new Borrowing(
                borrowingDto.getId(),
                borrowingDto.getBorrowDate(),
                borrowingDto.getReturnDate()
        );
    }
    public BorrowingDto mapToBorrowingDto(final Borrowing borrowing) {
        BorrowingDto borrowingDto = new  BorrowingDto(
                borrowing.getId(),
                borrowing.getBorrowDate(),
                borrowing.getReturnDate()
        );
        if(borrowing.getExemplar() != null) {
            borrowingDto.setExemplarId(borrowing.getExemplar().getId());
        }
        if(borrowing.getBorrower() != null) {
            borrowingDto.setBorrowerId(borrowing.getBorrower().getId());
        }
        return borrowingDto;
    }

    public List<Borrowing> mapToBorrowingList(final List<BorrowingDto> borrowingDtoList) {
        return borrowingDtoList.stream()
                .map(this::mapToBorrowing)
                .collect(Collectors.toList());
    }

    public List<BorrowingDto> mapToBorrowingDtoList(final List<Borrowing> borrowingList) {
        return borrowingList.stream()
                .map(this::mapToBorrowingDto)
                .collect(Collectors.toList());
    }
}
