package com.library.domain;

public enum Status {
    AVAILABLE,
    BORROWED,
    LOST,
    DAMAGED
}
