package com.library.controller;

import com.library.dto.BorrowingDto;
import com.library.exception.CannotPerformActionException;
import com.library.mapper.BorrowingMapper;
import com.library.service.BorrowingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("v1/borrowing")
@Slf4j
public class BorrowingController {
    @Autowired
    private BorrowingService borrowingService;
    @Autowired
    private BorrowingMapper borrowingMapper;

    @PostMapping("borrow")
    public BorrowingDto borrowBook(@RequestParam Long exemplarId, @RequestParam Long borrowerId) throws CannotPerformActionException {
        log.info("Borrow exmplar {} by borrower {}", exemplarId, borrowerId);
        return borrowingService.borrowBook(exemplarId, borrowerId);
    }

    @PutMapping("return")
    public BorrowingDto returnBook(@RequestParam Long exemplarId) throws CannotPerformActionException {
        log.info("Return exemplar ID = {}", exemplarId);
        return borrowingService.returnBook(exemplarId);
    }

    @GetMapping("{borrowingId}")
    public BorrowingDto getBorrowing(@PathVariable Long borrowingId) throws CannotPerformActionException {
        log.info("Get borrowing with ID = {}", borrowingId);
        return borrowingMapper.mapToBorrowingDto(borrowingService.getBorrowing(borrowingId).orElseThrow(() -> new CannotPerformActionException("Cannot find borrowing with this ID")));
    }

    @GetMapping("all")
    public List<BorrowingDto> getAllBorrowings() {
        log.info("Get all borrowings");
        return borrowingMapper.mapToBorrowingDtoList(borrowingService.getAllBorrowings());
    }
}
