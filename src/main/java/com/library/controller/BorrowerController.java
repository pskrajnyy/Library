package com.library.controller;

import com.library.dto.BorrowerDto;
import com.library.exception.CannotPerformActionException;
import com.library.mapper.BorrowerMapper;
import com.library.service.BorrowerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("v1/borrower")
@Slf4j
public class BorrowerController {
    @Autowired
    private BorrowerService borrowerService;
    @Autowired
    private BorrowerMapper borrowerMapper;

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public void createBorrower(@RequestBody BorrowerDto borrowerDto) {
        log.info("Create borrower: {} {}",borrowerDto.getFirstname(), borrowerDto.getLastname());
        borrowerService.saveBorrower(borrowerMapper.mapToBorrower(borrowerDto));
    }

    @GetMapping("{borrowerId}")
    public BorrowerDto getBorrower(@PathVariable Long borrowerId) throws CannotPerformActionException {
        log.info("Get borrower with ID = {}", borrowerId);
        return borrowerMapper.mapToBorrowerDto(borrowerService.getBorrower(borrowerId).orElseThrow(() -> new CannotPerformActionException("Cannot find borrower with this ID")));
    }

    @GetMapping("all")
    public List<BorrowerDto> getAllBorrowers() {
        log.info("Get all borrowers");
        return borrowerMapper.mapToBorrowerDtoList(borrowerService.getAllBorrowers());
    }

    @DeleteMapping("{borrowerId}")
    public void deleteBorrower(@PathVariable Long borrowerId) {
        log.info("Delete borrower with ID = {}", borrowerId);
        borrowerService.deleteBorrowerById(borrowerId);
    }

    @PutMapping
    public BorrowerDto updateBorrower(@RequestBody BorrowerDto borrowerDto) {
        log.info("Update borrower with ID = {}", borrowerDto.getId());
        return borrowerMapper.mapToBorrowerDto(borrowerService.saveBorrower(borrowerMapper.mapToBorrower(borrowerDto)));
    }
}
