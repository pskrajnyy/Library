package com.library.controller;

import com.library.domain.Status;
import com.library.dto.ExemplarDto;
import com.library.exception.CannotPerformActionException;
import com.library.mapper.ExemplarMapper;
import com.library.service.ExemplarService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("v1/exemplar")
@Slf4j
public class ExemplarController {
    @Autowired
    private ExemplarService exemplarService;
    @Autowired
    private ExemplarMapper exemplarMapper;

    @PostMapping("add")
    public void addExemplar(@RequestParam Long titleId, @RequestParam Status status) throws CannotPerformActionException {
        log.info("Add exemplar {} with status {}", titleId, status);
        exemplarService.addExemplar(titleId, status);
    }

    @GetMapping("exemplars")
    public long getAvailableExemplars(@RequestParam Long titleId) throws CannotPerformActionException {
        log.info("Get available exemplars book {}", titleId);
        return exemplarService.getAvailableExemplars(titleId);
    }

    @PutMapping("update")
    public ExemplarDto updateExemplarStatus(@RequestParam Long exemplarId, @RequestParam Status status) throws CannotPerformActionException {
        log.info("Update exemplars with ID = {}", exemplarId);
        return exemplarService.updateExemplarStatus(exemplarId, status);
    }

    @GetMapping("all")
    public List<ExemplarDto> getAllExemplars() {
        log.info("Get all exemplars");
        return exemplarMapper.mapToExemplarDtoList(exemplarService.getAllExemplars());
    }

    @GetMapping("{exemplarId}")
    public ExemplarDto getExemplar(@PathVariable Long exemplarId) throws CannotPerformActionException {
        log.info("Get exemplar with ID = {}", exemplarId);
        return exemplarMapper.mapToExemplarDto(exemplarService.getExemplar(exemplarId).orElseThrow(() -> new CannotPerformActionException("Cannot find exemplar with this ID")));
    }
}
