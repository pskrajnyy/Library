package com.library.controller;

import com.library.dto.TitleDto;
import com.library.exception.CannotPerformActionException;
import com.library.mapper.TitleMapper;
import com.library.service.TitleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("v1/title")
@Slf4j
public class TitleController {
    @Autowired
    private TitleService titleService;
    @Autowired
    private TitleMapper titleMapper;

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public void addTitle(@RequestBody TitleDto titleDto) throws CannotPerformActionException {
        log.info("Add title {}", titleDto.getTitle());
        titleService.addTitle(titleMapper.mapToTitle(titleDto));
    }

    @GetMapping("{titleId}")
    public TitleDto getTitle(@PathVariable Long titleId) throws CannotPerformActionException {
        log.info("Get title with ID = {}", titleId);
        return titleMapper.mapToTitleDto(titleService.getTitle(titleId).orElseThrow(() -> new CannotPerformActionException("Cannot find title with this ID")));
    }

    @GetMapping("all")
    public List<TitleDto> getTitles() {
        log.info("Get all titles");
        return titleMapper.mapToTitleDtoList(titleService.getAllTitle());
    }

    @DeleteMapping("{titleId}")
    public void deleteTitle(@PathVariable Long titleId) {
        log.info("Delete title with ID = {}", titleId);
        titleService.deleteTitleById(titleId);
    }
}
