package com.library.dto;

import com.library.domain.Status;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExemplarDto {
    private Long id;
    private Long titleId;
    private Status status;
    private List<BorrowingDto> borrowings = new ArrayList<>();

    public ExemplarDto(Long id, Status status, List<BorrowingDto> borrowings) {
        this.id = id;
        this.status = status;
        this.borrowings = borrowings;
    }
}
