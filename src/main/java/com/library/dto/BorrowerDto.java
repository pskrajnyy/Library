package com.library.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BorrowerDto {
    private Long id;
    private String firstname;
    private String lastname;
    private LocalDate accountCreationDate;
    private List<BorrowingDto> borrowings;

    public BorrowerDto(Long id, String firstname, String lastname, LocalDate accountCreationDate) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.accountCreationDate = accountCreationDate;
    }
}
