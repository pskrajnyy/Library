package com.library.service;

import com.library.domain.Title;
import com.library.exception.CannotPerformActionException;
import com.library.mapper.TitleMapper;
import com.library.repository.TitleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Service
public class TitleService {
    @Autowired
    TitleRepository titleRepository;
    @Autowired
    TitleMapper titleMapper;

    public List<Title> getAllTitle() {
        return titleRepository.findAll();
    }

    public Optional<Title> getTitle(final Long titleId) {
        return titleRepository.findById(titleId);
    }

    public void deleteTitleById(Long titleId) {
        titleRepository.deleteById(titleId);
    }

    public Title addTitle(final Title title) throws CannotPerformActionException {
        if(titleRepository.findAll().stream()
        .anyMatch(t -> t.getTitle().equals(title.getTitle()) && t.getAuthor().equals(title.getAuthor()) && t.getYearOfPublication() == title.getYearOfPublication()))
        {
            throw new CannotPerformActionException("This title is already in database");
        }
        return titleRepository.save(title);
    }
}
