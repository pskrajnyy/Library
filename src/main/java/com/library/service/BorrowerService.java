package com.library.service;

import com.library.domain.Borrower;
import com.library.repository.BorrowerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BorrowerService {
    @Autowired
    BorrowerRepository borrowerRepository;

    public List<Borrower> getAllBorrowers() {
        return borrowerRepository.findAll();
    }

    public Optional<Borrower> getBorrower(final Long borrowerId) {
        return borrowerRepository.findById(borrowerId);
    }

    public Borrower saveBorrower(final Borrower borrower) {
        return borrowerRepository.save(borrower);
    }

    public void deleteBorrowerById(Long borrowerId) {
        borrowerRepository.deleteById(borrowerId);
    }
}
