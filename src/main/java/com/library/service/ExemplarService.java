package com.library.service;

import com.library.domain.Exemplar;
import com.library.domain.Status;
import com.library.domain.Title;
import com.library.dto.ExemplarDto;
import com.library.exception.CannotPerformActionException;
import com.library.mapper.ExemplarMapper;
import com.library.repository.ExemplarRepository;
import com.library.repository.TitleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ExemplarService {
    @Autowired
    ExemplarRepository exemplarRepository;
    @Autowired
    TitleRepository titleRepository;
    @Autowired
    ExemplarMapper exemplarMapper;

    public ExemplarDto addExemplar(final Long titleId, final Status status) throws CannotPerformActionException {
        Exemplar exemplar = new Exemplar(status);
        exemplar.setTitle(titleRepository.findById(titleId).orElseThrow(() -> new CannotPerformActionException("Cannot find this title in database")));
        exemplar.getTitle().addExemplar(exemplar);
        return exemplarMapper.mapToExemplarDto(exemplarRepository.save(exemplar));
    }

    public ExemplarDto updateExemplarStatus(final Long id, final Status status) throws CannotPerformActionException {
        Exemplar exemplar = exemplarRepository.findById(id)
                .orElseThrow(() -> new CannotPerformActionException("Cannot find this exemplar in database"));
        exemplar.setStatus(status);
        return exemplarMapper.mapToExemplarDto(exemplarRepository.save(exemplar));
    }

    public long getAvailableExemplars(final Long titleId) throws CannotPerformActionException {
        Title title = titleRepository.findById(titleId).orElseThrow(() -> new CannotPerformActionException("Cannot find this title in database"));
        return exemplarRepository.countAllByStatusAndTitle(Status.AVAILABLE, title);
    }

    public List<Exemplar> getAllExemplars() {
        return exemplarRepository.findAll();
    }

    public Optional<Exemplar> getExemplar(final Long exemplarId) {
        return exemplarRepository.findById(exemplarId);
    }
}
