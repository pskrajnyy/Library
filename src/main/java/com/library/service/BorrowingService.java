package com.library.service;

import com.library.domain.Borrower;
import com.library.domain.Borrowing;
import com.library.domain.Exemplar;
import com.library.domain.Status;
import com.library.dto.BorrowingDto;
import com.library.exception.CannotPerformActionException;
import com.library.mapper.BorrowingMapper;
import com.library.repository.BorrowerRepository;
import com.library.repository.BorrowingRepository;
import com.library.repository.ExemplarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class BorrowingService {
    @Autowired
    BorrowingRepository borrowingRepository;
    @Autowired
    ExemplarRepository exemplarRepository;
    @Autowired
    BorrowerRepository borrowerRepository;
    @Autowired
    BorrowingMapper borrowingMapper;

    public List<Borrowing> getAllBorrowings() {
        return borrowingRepository.findAll();
    }

    public Optional<Borrowing> getBorrowing(final Long borrowingId) {
        return borrowingRepository.findById(borrowingId);
    }

    public BorrowingDto borrowBook(final Long exemplarId, final Long borrowerId) throws CannotPerformActionException {
        Exemplar exemplar = exemplarRepository.findById(exemplarId).orElseThrow(() -> new CannotPerformActionException("Cannot find this exemplar in database"));
        Borrower borrower = borrowerRepository.findById(borrowerId).orElseThrow(() -> new CannotPerformActionException("Cannot find this borrower in database"));
        if (exemplar.getStatus().equals(Status.AVAILABLE)) {
            Borrowing borrowing = new Borrowing(LocalDate.now());
            borrowing.setExemplar(exemplar);
            borrowing.setBorrower(borrower);
            exemplar.addBorrowing(borrowing);
            borrower.addBorrowing(borrowing);
            exemplar.setStatus(Status.BORROWED);
            return borrowingMapper.mapToBorrowingDto(borrowingRepository.save(borrowing));
        } else {
            throw new CannotPerformActionException("This exemplar is not available to be borrowed");
        }
    }

    public BorrowingDto returnBook(final Long exemplarId) throws CannotPerformActionException {
        Exemplar exemplar = exemplarRepository.findById(exemplarId).orElseThrow(() -> new CannotPerformActionException("Cannot find this exemplar in database"));
        exemplar.setStatus(Status.AVAILABLE);
        Borrowing borrowing = Optional.ofNullable(exemplar.getBorrowings()
                .get(exemplar.getBorrowings().size() - 1))
                .orElseThrow(() -> new CannotPerformActionException("Cannot find this book borrowing"));
        borrowing.setReturnDate(LocalDate.now());
        return borrowingMapper.mapToBorrowingDto(borrowingRepository.save(borrowing));
    }


}
